<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answer extends Model
{

    // Links app/Http/answer.php - app/Http/question.php
    public function question() {
        return $this->belongsToMany('App\question', 'answer_question');
    }

    // Links app/Http/answer.php - app/Http/response.php
    public function response() {
        return $this->belongsToMany('App\response', 'answer_response');
    }
}
