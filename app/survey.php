<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\question;

class survey extends Model
{
    // Allows fillable fields from form
    protected $fillable = [
        'name',
        'description',
    ];
    
    // Links app/Http/survey.php - app/Http/question.php
    public function question() {
        return $this->belongsToMany('App\question', 'question_survey');
    }
    
    // Links app/Http/response.php - app/Http/response.php
    public function response() {
        return $this->belongsToMany('App\response', 'question_response');
    }
}


