<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\survey;
use App\question;
use App\answer;

class response extends Model
{

    // Links app/Http/response.php - app/Http/question.php
    public function question() {
        return $this->belongsToMany('App\question', 'question_response');
    }

    // Links app/Http/response.php - app/Http/answer.php
    public function answer() {
        return $this->belongsToMany('App\answer', 'answer_response');
    }
}
