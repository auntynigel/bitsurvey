<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    public function permissions() {
        return $this->belongsToMany(permission::class);
    }

    public function givePermissionTo(permission $permission) {
        return $this->permissions()->sync($permission);
    }
}
