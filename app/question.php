<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\answer;
use App\survey;

class question extends Model
{

    // Links app/Http/question.php - app/Http/survey.php
    public function survey() {
        return $this->belongsToMany('App\survey', 'question_survey');
    }

    // Links app/Http/question.php - app/Http/answer.php
    public function answer() {
        return $this->belongsToMany('App\answer', 'answer_question');
    }

    // Links app/Http/question.php - app/Http/response.php
    public function response() {
        return $this->belongsToMany('App\response', 'question_response');
    }
}
