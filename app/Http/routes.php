<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'HomeController');
Route::get('surveys', 'QuestController@index');
Route::resource('admin', 'AdminController@index');

Route::post('/survey/{{ $surveys->id }}/response', 'QuestController@store');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::resource('/admin/users', 'UserController' );
    Route::get('surveys/{survey}', 'QuestController@show');
    Route::resource('admin/create', 'CreateController');
});