<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\survey;
use App\question;
use App\answer;
use App\question_survey;
use App\response;

use App\Http\Requests;

class QuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $surveys = survey::all();

        return view('surveys', ['surveys' => $surveys]);
    }

    public function show($id)
    {
        $surveys = survey::find($id);
        $questions = survey::find($id)->question()->get();
        return view('surveys.questionnaire', ['surveys' => $surveys, 'questions' => $questions]);
    }

    public function store(Request $request, survey $survey, question $question, answer $answer)
    {

//        return $request->all();
        $add = new response;

        $add->survey_id = $survey->id;
        $add->question_id = $question->id;
        $add->answer_id = $answer->id;

        $add->save();

        return Redirect('surveys');

    }
}
