<header>
    <div class="row">
        <h1 class="text-center">BitSurvey</h1>
    </div>
</header>

<!-- sticky nav bar -->

<div class="contain-to-grid sticky">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area inline-list align-bottom">
            <li class="name">
                <h1><a href="{{ url('/') }}" alt="Home page link">Valuing information</a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li><a href="{{ url('/about') }}" alt="About Bitsurvey">About</a></li>
                <li><a href="{{ url('/contact') }}" alt="Contact Bitsurvey">Contact</a></li>
                <li><a href="{{ url('/surveys') }}" alt="Surveys">Surveys</a></li>

                <!-- drop menu for anon user-->
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}" alt="Register">Register</a></li>
                    <li><a href="{{ url('/login') }}" alt="Sign in">Sign in</a></li>
                @else
                    <li><a href="{{ url('/admin') }}" alt="Create new survey">Create Survey</a></li>
                    <li><a href="{{ url('/logout') }}" alt="Logout">{{ Auth::user()->name }}: Logout</a></li>
                @endif
            </ul>
        </section>

    </nav>
</div>