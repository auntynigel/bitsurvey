@extends('layouts.app')
@section('title', 'Surveys')
@section('content')

    <section>
        @if (isset ($surveys))

            <div class="row" id="surveys">

                <!-- run through all surveys and list each found -->
                @foreach ($surveys as $survey)
                    <a href="{{ url('/surveys') }}/{{ $survey->id }}" alt="link to survey {{ $survey->id }}">
                        <div class="small-3 columns survey ">
                            <h3>{{ $survey->name }}</h3>
                            <p>{{ $survey->description }}</p>
                        </div>
                    </a>
                @endforeach

            </div> <!-- end row -->

        @else
            <p>No surveys found</p>
        @endif
    </section>

@endsection