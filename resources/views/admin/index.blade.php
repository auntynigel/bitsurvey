@extends('layouts.app')
@section('title', 'Admin')
@section('content')

    <section>
        <div class="row">
            <div class="small-12 columns">
                <h1>Admin Area</h1>
                <hr>
            </div>
            <div class="small-12 column">
                {!! Form::open() !!}

                <div class="form-group">
                    {!! Form::label('name', 'Title:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Add skill', ['class' => 'btn btn-primary form-control']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>

    </section>

@endsection