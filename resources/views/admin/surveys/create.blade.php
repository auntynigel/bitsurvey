<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Survey</title>
</head>
<body>
<h1>Add Article</h1>

{!! Form::open(array('action' => 'SurveyController@store', 'id' => 'createsurvey')) !!}
{{ csrf_field() }}

<div class="row large-12 columns">
    {!! Form::label('name', 'Title:') !!}
    {!! Form::text('name', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('description', 'escription:') !!}
    {!! Form::textarea('description', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-4 columns">
    {!! Form::submit('Add Survey', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}

</body>
</html>