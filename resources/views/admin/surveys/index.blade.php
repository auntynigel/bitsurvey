<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>All Users</title>
    <link href="favicon.ico" rel="icon" type="image/x-icon" />
</head>
<body>
<h1>All Users</h1>

<section>
    @if (isset ($surveys))

        <div class="row">

            <!-- run through all surveys and list each found -->
            @foreach ($surveys as $survey)
                <a href="/admin/surveys/{{ $user->id }}" name="{{ $survey->name }}>"
                    <div class="small-4 columns">
                        <h3>{{ $survey->name }}</h3>
                        <p>{{ $survey->description }}</p>
                    </div>
                </a>
            @endforeach

        </div> <!-- end row -->

    @else
        <p>No surveys found</p>
    @endif
</section>

</body>
</html>