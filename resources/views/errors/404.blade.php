@extends('layouts.app')
@section('title', 'Error 404!')
@section('content')
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h1 class="text-center">404!</h1>
                <p class="text-center">Oops! It seems this page doesn't exist.</p>
                <p>Perhaps we broke something, maybe you did something wrong? Lets not play the blame game. <a href="{{ URL::previous() }}" alt="Link to previous page">GO BACK</a></p>
            </div>

        </div>

    </section>