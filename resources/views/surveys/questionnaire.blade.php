@extends('layouts.app')
@section('title', 'Questionnaire')
@section('content')

    <div class="row">
        <h1>Welcome to survey {{ $surveys->name }}</h1>
        <p>{{ $surveys->description }}</p>
    </div>

    @if (isset ($questions))

        <div class="row">
            <!-- Is survey open (1) -->
            @if ($surveys->open === '1')

                <!-- run through all questions found -->
                <ol>
                    <form method="POST" action="/survey/{{ $surveys->id }}/response">
                    @foreach ($questions as $question)
                        <li>{{ $question->question_text }}</li>

                        <!-- If question id (age) -->
                        @if ($question->id === 1)
                            <p><input type="radio" name="{{ $question->id }}" value="4" checked> 25 or under</p>
                            <p><input type="radio" name="{{ $question->id }}" value="5"> 26 and Over</p>
                            {{--@foreach($answers-id as $answer)--}}
                                {{--<p><input type="radio" name="{{ $question->id }}" value="{{ $answer->answer_text }}" checked> {{ $answer->answer_text }}</p>--}}
                            {{--@endforeach--}}

                        <!-- All other question / answer choices -->
                        @else
                            <p><input type="radio" name="{{ $question->id }}" value="1" checked> Yes</p>
                            <p><input type="radio" name="{{ $question->id }}" value="2"> No</p>

                        @endif <!-- close if (question->id === 1) -->
                        <hr>

                    <!-- end foreach $question -->
                    @endforeach
                    <input type="submit" name="Submit" class="button round">
                    </form>

                </ol>

            <!-- if survey shut (open = 0) -->
            @else
                <h3>Oops! Something seems wrong.</h3>
                <p>It seems this survey is now closed <a href="{{ URL::previous() }}" alt="return to previous page">Go Back</a></p>

            <!-- Closes (open === 1) -->
            @endif
        </div> <!-- end row -->

    <!-- display if no questions -->
    @else
        <p>Oops! No questions found</p>
    @endif

@endsection