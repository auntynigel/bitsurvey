@extends('layouts.app')

@section('title', 'BitSurvey')

@section('content')

<div class="container">
    <div class="row">

        <div class="small-4 columns">
            <p class="text-center"><i class="fa fa-bar-chart fa-3x"></i></p>
            <p class="text-justify">Powerful survey reporting tools including real time reporting and graphical representation of data gathered</p>
        </div>
        <div class="small-4 columns">
            <p class="text-center"><i class="fa fa-check fa-3x"></i></p>
            <p class="text-justify">Help receive quick and accurate market research direct from your demographics</p>
        </div>
        <div class="small-4 columns">
            <p class="text-center"><i class="fa fa-thumbs-o-up fa-3x"></i></p>
            <p class="text-justify">Create bespoke surveys to gather feedback on customer satisfaction</p>
        </div>

    </div>
</div>
@endsection
