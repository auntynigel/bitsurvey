<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
$this->call(QuestionsTableSeeder::class);
$this->call(AnswersTableSeeder::class);
$this->call(SurveysTableSeeder::class);
$this->call(UsersTableSeeder::class);
$this->call(RolesTableSeeder::class);
$this->call(PermissionsTableSeeder::class);
$this->call(Answer_QuestionsTableSeeder::class);
$this->call(Permission_RolesTableSeeder::class);
$this->call(Question_SurveysTableSeeder::class);
$this->call(Role_UsersTableSeeder::class);
$this->call(SurveysTableSeeder::class);
    }
}
