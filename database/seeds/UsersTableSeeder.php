<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Mark Williams', 'email' => '22687092@go.edgehill.ac.uk', 'password'=>bcrypt('webdev')],
            ['name' => 'test user', 'email' => 'test@go.edgehill.ac.uk', 'password'=>bcrypt('webdev')],
            ['name' => 'test user2', 'email' => 'test2@go.edgehill.ac.uk', 'password'=>bcrypt('webdev')],
        ]);
    }
}
