<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['role' => 'Admin', 'description' => 'Admin'],
            ['role' => 'User', 'description' => 'User'],
            ['role' => 'Editor', 'description' => 'Editor'],
        ]);
    }
}
