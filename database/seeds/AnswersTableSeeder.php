<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            ['answer_text' => 'Yes'],
            ['answer_text' => 'No'],
            ['answer_text' => "Don't know"],
            ['answer_text' => '25 or under'],
            ['answer_text' => '26 or older'],
        ]);
    }
}
