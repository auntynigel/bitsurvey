<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['permissions' => 'all', 'description' => 'Admin'],
            ['permissions' => 'view', 'description' => 'User'],
            ['permissions' => 'amend', 'description' => 'Editor'],
        ]);
    }
}
