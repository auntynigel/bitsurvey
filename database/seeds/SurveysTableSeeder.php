<?php

use Illuminate\Database\Seeder;

class SurveysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surveys')->insert([
            ['name' => 'General intro', 'description' => 'An example survey with simple questions as proof of concept', 'open' => 1],
            ['name' => 'Social survey', 'description' => 'How do you find the night life in the town surrounding the university', 'open' => 0],
            ['name' => 'A closed survey', 'description' => 'Just another closed survey', 'open' => 0],
            ['name' => 'Another closed survey', 'description' => 'Just yet another closed survey', 'open' => 0],
            ['name' => 'Mobile internet usage', 'description' => 'Understanding how people use their mobile phone internet', 'open' => 0],
        ]);
    }
}
