<?php

use Illuminate\Database\Seeder;

class Question_SurveysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_survey')->insert([
            ['survey_id' => 1, 'question_id' => 1],
            ['survey_id' => 1, 'question_id' => 2],
            ['survey_id' => 1, 'question_id' => 3],
            ['survey_id' => 1, 'question_id' => 4],
            ['survey_id' => 1, 'question_id' => 5],
        ]);
    }
}
