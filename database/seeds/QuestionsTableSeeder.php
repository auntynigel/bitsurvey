<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['question_text' => 'How old were on your last birthday?'],
            ['question_text' => 'Are you currently a student?'],
            ['question_text' => 'Do you attend more than 50% of lectures?'],
            ['question_text' => 'Do you attend more than 50% of seminar sessions?'],
            ['question_text' => 'Do you prefer lectures over seminars?'],
        ]);
    }
}
