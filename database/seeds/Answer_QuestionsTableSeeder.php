<?php

use Illuminate\Database\Seeder;

class Answer_QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer_question')->insert([
            ['question_id' => 1, 'answer_id' => 4],
            ['question_id' => 1, 'answer_id' => 5],
            ['question_id' => 2, 'answer_id' => 1],
            ['question_id' => 2, 'answer_id' => 2],
            ['question_id' => 3, 'answer_id' => 1],
            ['question_id' => 3, 'answer_id' => 2],
            ['question_id' => 4, 'answer_id' => 1],
            ['question_id' => 4, 'answer_id' => 2],
            ['question_id' => 5, 'answer_id' => 1],
            ['question_id' => 5, 'answer_id' => 2],
        ]);
    }
}
