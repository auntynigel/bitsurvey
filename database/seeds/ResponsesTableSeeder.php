<?php

use Illuminate\Database\Seeder;

class ResponsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('responses')->insert([
            ['survey_id' => 1, 'question_id' => 1, 'answer_id' => 4],
            ['survey_id' => 1, 'question_id' => 2, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 3, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 4, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 5, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 1, 'answer_id' => 5],
            ['survey_id' => 1, 'question_id' => 2, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 3, 'answer_id' => 2],
            ['survey_id' => 1, 'question_id' => 4, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 5, 'answer_id' => 2],
            ['survey_id' => 1, 'question_id' => 1, 'answer_id' => 4],
            ['survey_id' => 1, 'question_id' => 2, 'answer_id' => 2],
            ['survey_id' => 1, 'question_id' => 3, 'answer_id' => 1],
            ['survey_id' => 1, 'question_id' => 4, 'answer_id' => 2],
            ['survey_id' => 1, 'question_id' => 5, 'answer_id' => 1],
        ]);
    }
}
